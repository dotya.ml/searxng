# [searxng](https://git.dotya.ml/dotya.ml/searxng)

this repo holds configs of [dotya.ml's SearxNG instance](https://searxng.dotya.ml/).

see what engines are configured in the ENGINES tab of
[preferences](https://searxng.dotya.ml/searxng/preferences).

### LICENSE
WTFPLv2, see [LICENSE](LICENSE) for details
